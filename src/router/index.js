import Vue from 'vue'
// import App from '@/App'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Home from '@/components/home/Home'
import Login from '@/components/Login'
import Member from '../page/controller/sysmgt/member'
import Dept from '../page/controller/sysmgt/department'
Vue.use(Router)

export default new Router({
  mode: 'history', // 去掉url中的#
  routes: [
    //   {
    //   path: '/',
    //   name: '',
    //   component: App,
    //   meta: {
    //     keepAlive: true
    //   }
    // },
    {
      path: '/',
      redirect: '/login',
      component: Login,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/helloworld',
      name: 'helloworld',
      component: HelloWorld
    },
    {
      path: '/ctr/sysmgt/member',
      name: 'member',
      component: Member,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/ctr/sysmgt/department',
      name: 'dept',
      component: Dept,
      meta: {
        keepAlive: true
      }
    }
  ]
})
