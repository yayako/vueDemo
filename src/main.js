// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import qs from 'qs'
import MintUI from 'mint-ui'
// import VeeValidate from 'vee-validate'
import ElementUI from 'element-ui'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'
import 'mint-ui/lib/style.css'
import 'element-ui/lib/theme-chalk/index.css'
// const config = {
//   locale: 'zh_CN'
// }
// Vue.use(VeeValidate, config)
Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.prototype.$qs = qs
Vue.use(MintUI)
Vue.use(ElementUI)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
